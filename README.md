# Czym jest ten tekst? #

Opis konwencji i dobrych praktyk dla trello

### Co umieszczamy w tym opisie? ###

* Dobre praktyki, które ułatwiają komunikację i pracę dev - business
* Konwencje dotyczące oznaczania tasków, tablic oraz nazewnictwa

### Treść opisu karty ###

Dla spójności stosujemy opis taska w danym schemacie:
Kod ten należy skopiować i umieścić w sekcji "opis" karty zadania.
Po zapisaniu, zostanie on przetworzony i wyrenderowany 


	### OPIS: 

	Błąd przy zmianie zdjęcia prelegenta

	---
	### GDZIE: [Kliknij by przejść do problemu](https://www.google.pl/#q=mi%C5%82o,+%C5%BCe+jeste%C5%9B)

	---
	### KROKI:

	1. Zaloguj się do inside
	1. Przejdź do sekcji 'prelegenci'
	1. Wrzuć zdjęcie w formacie .jpg

	---
	### SCREEN:

	![Sekcja](https://image.prntscr.com/image/a72d3ce55ab14c4288dc60f6d85dc1ea.png)



Co będzie wyglądać jak to:



`
### OPIS: 

Błąd przy zmianie zdjęcia prelegenta

---
### GDZIE: [Kliknij by przejść do problemu](https://www.google.pl/#q=mi%C5%82o,+%C5%BCe+jeste%C5%9B)

---
### KROKI:

1. Zaloguj się do inside
1. Przejdź do sekcji 'prelegenci'
1. Wrzuć zdjęcie w formacie .jpg

---
### SCREEN:

![Sekcja](https://image.prntscr.com/image/a72d3ce55ab14c4288dc60f6d85dc1ea.png)

`


Co jest łatwe do przeczytania :)


Jeśli jakaś sekcja nie ma sensu dla danej karty, należy ją pominąć.

Jeśli cały task mieści się w opisie, dodajemy liste z nazwą "status".
Ma to na celu śledzenie stanu taska. Ten czasem przeniesiony jest na
"done", a potem na "to do" i po pół roku nie wiadomo czy został zrobiony czy nie.

Jesli zadań jest kilka, tworzymy listę dla każdego zadania a w sekcji "opis"
tworzymy szablon, bez 
"### KROKI".
Dla każdego zadania dajemy krótki opis błędu z linkiem, albo nazwą przycisku, który sprawia problem.

### Lista ma za zadanie informować o tym czy dany task task został rozwiązany, przez kogo i kiedy

### Etykiety ###

Need business update - oznacza, że dany task wymaga sprostowania, wyjaśnienia. 
Dla tej etykiety należy w komentarzu otagować osobę, która ma za zadanie uzupełnić informacje
oraz podać treść, lub numer zadania na liście. Należy pamiętać o wskazaniu niejasności :)

### Karty ###

Do każdej karty przypisujemy developera.
Jeśli nie wiadomo komu przydzielić zadanie, należy je przypisać do scrum mastera,
i dodać etykietę "Need task evaluation", a ten powinien przydzielić zadanie do odpowiedniej 
osoby.
Wszystko wrzucone zostało do wspólnych list.
Etykiety i określenie członków w karcie pozwala na wygodną filtrację.

	W sekcji menu wybierz fitruj karty i zaznacz interesujące osoby.


### Sortowanie zadań ###

Zadania sortowane są w dwóch płaszczyznach:

1. Pierwsza - numeracja sprintu
2. Druga - etykiety "High priority", "Important", "Nothing necessary"


### Estymacja zadań ###

* Zainstaluj dodatek do chrome [link](https://chrome.google.com/webstore/detail/scrum-for-trello/jdbcdblgjdpmfninkoogcfpnkjmndgje)
* Na stronie trello zauważysz dane o przewidywanym czasie wykonania taska i podsumowaniu czasu dla całej karty
* Dane te mozna zmieniać klikając na karte i edytując jej nazwę, przejść na koniec linii tytułu. Powinny się pojawić przyciski z wyborem czasu

### Linkowanie brancha ###

W karcie widoczny jest przycisk bitbucket.
Rozpoczynając prace nad taskiem należy utworzyć repo
dla danego taska. Dzięki temu łatwo można dojść do tego
jaki kod jest odpowiedzialny za pokrycie treści zadania
i ew. analize blędów jakie powstały w wyniku wdrożenia zadania